# Checklist (make sure you check all the steps)

- [ ] Give your issue a title (pillar name - short description)
- [ ] Fill in the template question
- [ ] Select your pillar with Epic (if your request is not in a pillar select Other)
- [ ] Set the priority with labels (~"prio:2", ~"prio:2", ~"prio:3")
- [ ] Select create issue


## Wich dashboard you want to implement a change?

(Here you should put the name of the dashboard you want to change)

## Summary of the dashboard change

(Summarize the change in short detail, what do you want to add or remove?)

## Why do you want to change this dashboard?

(Give a reason why you want to implement your change, what are the leading points?)

## Example Project

